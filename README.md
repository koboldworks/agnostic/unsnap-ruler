# Unsnap Ruler

A micro module that disables ruler snapping when shift is held in Foundry Virtual Tabletop.

![Waypoint Example](./img/screencaps/waypoints.png)

This is specifically for ruler. Templates are unaffected.

## Known Issues

- Mixing snapped and unsnapped measurement provides correct measured distances only while shift is held. Cause unknown.  
- Grid highlights are weird if you measure along grid edges. This should be expected. It won't be fixed.

## Install

Manifest URL: <https://gitlab.com/koboldworks/agnostic/unsnap-ruler/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
