# Changelog

## 1.0.1

- Maintenance release.
- Foundry v11 (b292 Prototype 1) compatibility confirmed

## 1.0.0

- Maintenance and FoundryVTT v10 compatibility confirmation.

## 0.1.1

- Minor under the hood improvements.
- Repository moved to [gitlab.com/koboldworks/agnostic/unsnap-ruler](https://gitlab.com/koboldworks/agnostic/unsnap-ruler)

## 0.1.0

- Initial release. Base implementation.
